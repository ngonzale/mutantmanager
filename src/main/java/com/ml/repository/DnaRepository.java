package com.ml.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ml.domain.DNA;

/**
 * DNA repository layer
 * 
 * @author nicolas.gonzalez
 *
 */
@Repository
public interface DnaRepository extends CrudRepository<DNA, Integer> {

    /**
     * get mutants or not qty due given parameter
     * 
     * @param isMutantDNA
     *            {@link Boolean}
     * @return {@link Integer}
     */
    @Query("select count(*) from DNA d where d.isMutantDNA = ?1")
    Integer findByMutantCondition(final boolean isMutantDNA);

}
