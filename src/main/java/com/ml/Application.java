package com.ml;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
/**
 * Main spring boot application
 * @author nicolas.gonzalez
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories
public class Application {
    /**
     * Entry spring boot point
     * @param args string array 
     * @throws Exception {@link Exception}
     */
    public static void main(final String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
