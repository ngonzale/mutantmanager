package com.ml.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ml.domain.Person;
import com.ml.repository.DnaRepository;

/**
 * Mutant REST endpoint
 * 
 * @author n.gonzalez
 *
 */
@RestController
public class MutantResource {

    @Autowired
    private DnaRepository dnaRepository;
    
    /**
     * check if a person is mutant or not due his DNA
     * 
     * @param person
     *            {@link Person} to check nature
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/mutant", method = RequestMethod.POST)
    public ResponseEntity<?> mutant(@RequestBody final Person person) {
        
        Boolean isMutant = person.isMutant();
        
        dnaRepository.save(person.getAdn());
        
        if (isMutant) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    /**
     * get mutants qty ratio
     * 
     * @return {@link ResponseEntity}
     */
    @RequestMapping(method = RequestMethod.GET, name = "/stats", produces = "application/json")
    public ResponseEntity<Float> getStats() {
        
        Float ratio = calculateRatio();
        
        return ResponseEntity.ok(ratio);
    }
    
    /**
     * Calculate ratio of persons with mutant dna
     * 
     * @return {@link Double}
     */
    protected Float calculateRatio() {
        
        // move to service
        Long count = dnaRepository.count();
        Integer mutants = dnaRepository.findByMutantCondition(true);
        
        if (count == 0 || mutants == 0) {
            return 0F;
        }
        
        return Float.valueOf(mutants) / Float.valueOf(count);
    }
}
