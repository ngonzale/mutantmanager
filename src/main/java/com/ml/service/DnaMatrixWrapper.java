package com.ml.service;

import static com.ml.service.DnaMatrixWrapper.Directions.DIAGONAL_LEFT;
import static com.ml.service.DnaMatrixWrapper.Directions.DIAGONAL_RIHT;
import static com.ml.service.DnaMatrixWrapper.Directions.HORIZONTAL;
import static com.ml.service.DnaMatrixWrapper.Directions.VERTICAL;


import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


/**
 * Wrapper for dna matrix with backtracking methods
 * @author nicolas.gonzalez
 *
 */
public class DnaMatrixWrapper {

	private char[][] matrix;
	private List<Character> dnaFound = new LinkedList<Character>();
	private HashSet<Character> validCharacters;
	//MAKE THIS CONFIGURABLE
	private int MUTANT_DNA_CRITERIA = 3;//index begin from 0
	private Integer sequencyQuantity = 0;

    public DnaMatrixWrapper(String[] matrix) {
		//MAKE THIS CONFIGURABLE
		validCharacters = new HashSet<Character>(Arrays.asList(Character.valueOf('A'), 
															   Character.valueOf('T'), 
															   Character.valueOf('G'), 
															   Character.valueOf('C')));
		this.matrix = buildInternalMatrix(matrix);
	}
	

	/**
	 * Setter
	 * @param initialMatrix array of {@link String}
	 * @return matrix in char[][] format (easier to reference)
	 */
	protected char[][] buildInternalMatrix(String[] initialMatrix) {
		
		this.matrix = new char[initialMatrix.length][initialMatrix.length];

		//Validate the input matrix, and build an internal one
		for (int i = 0; i < initialMatrix.length; i++) {
			String string = initialMatrix[i];
			
			validateSequenceSize(string, initialMatrix.length);

			for (int j = 0; j < string.length(); j++) {

				char charAt = string.charAt(j);

				validateCharacter(charAt);

				this.matrix[i][j] = charAt;

			}
		}
		return matrix;
	}

	/**
	 * validate dna elements size
	 * @param string dna element
	 * @param length dna correct size
	 */
	private void validateSequenceSize(String string, int length) {
		if (string.length() != length) {
			throw new IllegalArgumentException("Invalid DNA sequence:" + string);
		}
	}

	/**
	 * Check if is an allowed character due validCharacters collection
	 * @param charAt char to be validated
	 */
	private void validateCharacter(char charAt) {
		if (!this.validCharacters.contains(charAt)) {
			throw new IllegalArgumentException("Invalid DNA character:" + charAt);
		}
	}

	/**
	 * Backtracking internal method
	 * @return {@link Boolean}
	 */
	public boolean isMutantDna() {
		int N = matrix.length;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (search(matrix, matrix[i][j], i, j, 0, N)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Backtracking algorithm adaptation to search dna patterns
	 * @param matrix dna config characters
	 * @param letter letter what we are looking for linear pattern
	 * @param row used internally by the algorithm
	 * @param col used internally by the algorithm
	 * @param index used internally by the algorithm
	 * @param N used internally by the algorithm
	 * @param direction used internally by the algorithm
	 * @return {@link Boolean}
	 */
	private boolean search(char[][] matrix, char letter, int row, int col, int index, int N, int direction) {

		// check if current cell not already used or character in it is not not
		if (letter != matrix[row][col]) {
			return false;
		}

		if (index == MUTANT_DNA_CRITERIA) {
			// dna element is found, return true
			return true;
		}

		if (dnaFound.contains(letter)) {
			return false;
		}

		if ((direction == VERTICAL.ordinal()) && (row + 1 < N && search(matrix, letter, row + 1, col, index + 1, N, VERTICAL.ordinal()))) { // go
			// down
			return true;
		}

		if ((direction == HORIZONTAL.ordinal())
				&& (col + 1 < N && search(matrix, letter, row, col + 1, index + 1, N, HORIZONTAL.ordinal()))) { // go
			// right
			return true;
		}

		if ((direction == DIAGONAL_RIHT.ordinal()) && (row + 1 < N && col + 1 < N
				&& search(matrix, letter, row + 1, col + 1, index + 1, N, Directions.DIAGONAL_RIHT.ordinal()))) {
			// go diagonally down right
			return true;
		}
		if ((direction == DIAGONAL_LEFT.ordinal()) && (row + 1 < N && col - 1 >= 0
				&& search(matrix, letter, row + 1, col - 1, index + 1, N, DIAGONAL_LEFT.ordinal()))) {
			// go diagonally down left
			return true;
		}

		// if none of the option works out, BACKTRACK and return false
		return false;
	}

	/**
	 * Backtracking algorithm adaptation to search dna patterns
	 * @param matrix dna config characters
	 * @param letter letter letter what we are looking for linear pattern
	 * @param row row used internally by the algorithm
	 * @param col row used internally by the algorithm
	 * @param index row used internally by the algorithm
	 * @param N row used internally by the algorithm
	 * @return {@link Boolean}
	 */
	public boolean search(char[][] matrix, char letter, int row, int col, int index, int N) {


		if (letter != matrix[row][col]) {
			return false;
		}

		if (dnaFound.contains(letter)) {
			return false;
		}

		if (index == MUTANT_DNA_CRITERIA) {
			// dna element is found, return true
			return true;
		}

		if (row + 1 < N && search(matrix, letter, row + 1, col, index + 1, N, VERTICAL.ordinal())) {
			if (this.sequencyQuantity == 0) {
				this.sequencyQuantity++;
				this.dnaFound.add(letter);
			} else {
				return true;
			}
		}
		if (col + 1 < N && search(matrix, letter, row, col + 1, index + 1, N, HORIZONTAL.ordinal())) {
			if (this.sequencyQuantity == 0) {
				this.sequencyQuantity++;
				this.dnaFound.add(letter);
			} else {
				return true;
			}
		}
		if (row + 1 < N && col + 1 < N && search(matrix, letter, row + 1, col + 1, index + 1, N, DIAGONAL_RIHT.ordinal())) {
			// go diagonally down right
			if (this.sequencyQuantity == 0) {
				this.sequencyQuantity++;
				this.dnaFound.add(letter);
			} else {
				return true;
			}
		}
		if (row + 1 < N && col - 1 >= 0 && search(matrix, letter, row + 1, col - 1, index + 1, N, DIAGONAL_LEFT.ordinal())) {
			// go diagonally down left
			if (this.sequencyQuantity == 0) {
				this.sequencyQuantity++;
				this.dnaFound.add(letter);
			} else {
				return true;
			}
		}

		// if none of the option works out, BACKTRACK and return false
		return false;
	}
	
	/**
	 * Possible direction in which cursor may go
	 * @author n.gonzalez
	 *
	 */
	public enum Directions {
		HORIZONTAL, VERTICAL, DIAGONAL_RIHT, DIAGONAL_LEFT;
	}
}
