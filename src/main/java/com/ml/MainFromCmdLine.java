package com.ml;

import java.util.Scanner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.ml.service.DnaMatrixWrapper;

/**
 * Simple entry poin to be able to test mutant recruiter
 * @author n.gonzalez
 *
 */
public class MainFromCmdLine {
	
	static Integer dimension = null;
	
	public static void main(String[] args) throws ParseException {
		
		setArgumentValues(args);
		
		String[] matrix = fillMatrix();
		
		DnaMatrixWrapper wrapper = new DnaMatrixWrapper(matrix);
		
		System.out.println("Is mutant: " + wrapper.isMutantDna());

	}

	/**
	 * Populate dna matrix
	 * @return string array
	 */
	private static String[] fillMatrix() {

		Scanner scanner = new Scanner(System.in);
		
		String[] matrix = new String[dimension];
		
		for (int i = 0; i < dimension; i++) {
			
			 String dnaElement = scanner.next();
			 if (dnaElement.length() > dimension) {
				 throw new IllegalArgumentException("Too much longer adn element. Max size : " + dimension);
			 }
			 
			 matrix[i] = dnaElement;
		}
		
		scanner.close();
		
		return matrix;
	}

	/**
	 * Set values from input stream
	 * @param args string array
	 * @throws ParseExceptio {@link ParseException}
	 */
	private static void setArgumentValues(String[] args) throws ParseException {

		CommandLine commandLine;
		Option operatorsOption = Option.builder("n").argName("matrix dimension").hasArg().type(Number.class).desc("dimension of the nxn dna matrix").required(true).build();
		
		Options options = new Options();
		CommandLineParser parser = new DefaultParser();

		HelpFormatter formatter = new HelpFormatter();
		String header = "\n -n dimension of the nxn dna matrix \n";
		String footer = "by nicolas.hernan.gonzalez@gmail.com";
		formatter.printHelp("java -jar MutantRecruiting.jar", header, options, footer, true);
		System.out.println();

		options.addOption(operatorsOption);
		commandLine = parser.parse(options, args);

		dimension = Integer.valueOf(commandLine.getOptionValue("n"));
        if (dimension < 4){
            throw new IllegalArgumentException("parametero dimension tiene que ser mayor o igual que 4");
        }
	}
	
	
}
