package com.ml.domain;

import java.util.List;

/**
 * Representation of human person wich have possible mutant dna
 * 
 * @author nicolas.gonzalez
 *
 */
public class Person {

    private DNA dna;

    /**
     * Determines if the persona have mutant dna or not
     * 
     * @return {@link Boolean}
     */
    public boolean isMutant() {
        return this.dna.isMutantDNA() ? dna != null : false;
    }

    /**
     * @param dnaParam
     *            the dna to set
     */
    public void setDna(final List<String> dnaParam) {
        if (dnaParam != null && !dnaParam.isEmpty()) {
            String[] array = dnaParam.toArray(new String[dnaParam.size()]);
            this.dna.setDnaConfig(array);
        }
    }

    /**
     * Default constructor
     */
    public Person() {
        super();
        this.dna = new DNA();
    }

    /**
     * @return the adn
     */
    public DNA getAdn() {
        return dna;
    }

    /**
     * @param adn
     *            the adn to set
     */
    public void setAdn(final DNA adn) {
        this.dna = adn;
    }
}
