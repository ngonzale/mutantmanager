package com.ml.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ml.service.DnaMatrixWrapper;

/**
 * Abstraction of an Human DNA which can be mutant or not
 * 
 * @author n.gonzalez
 *
 */
@NamedQuery(name = "DNA.findByMutantCondition", query = "select count(*) from DNA d where d.isMutantDNA = ?1")
@Entity
@Table(name = "DNA")
public class DNA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String[] dnaConfig;

    private Boolean isMutantDNA;

    @Transient
    private DnaMatrixWrapper matrixWrapper;

    /**
     * Default constructor
     */
    public DNA() {
        super();
    }

    /**
     * @return the dnaConfig
     */
    public String[] getDnaConfig() {
        return dnaConfig;
    }

    /**
     * @param dnaConfig
     *            the dnaConfig to set
     */
    public void setDnaConfig(final String[] dnaConfig) {
        this.dnaConfig = dnaConfig;
        this.matrixWrapper = new DnaMatrixWrapper(dnaConfig);
    }

    /**
     * @return the isMutantDNA
     */
    public Boolean isMutantDNA() {
        if (this.isMutantDNA == null) {
            this.isMutantDNA = matrixWrapper.isMutantDna();
        }
        return this.isMutantDNA;
    }

    /**
     * @param isMutantDNA
     *            the isMutantDNA to set
     */
    public void setMutantDNA(final Boolean isMutantDNA) {
        this.isMutantDNA = isMutantDNA;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

}
