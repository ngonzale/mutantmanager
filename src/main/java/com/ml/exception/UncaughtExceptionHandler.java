package com.ml.exception;

import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Exception handler
 * 
 * @author n.gonzalez
 *
 */
@ControllerAdvice
@Component
public class UncaughtExceptionHandler {

    /**
     * Handler for runtimeExceptions not catched for any reason in the app
     * 
     * @param excepcion {@link RuntimeException}
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<?> handleRuntimeException(final RuntimeException excepcion) {
        return new ResponseEntity<>(error(excepcion.getLocalizedMessage().toString()), HttpStatus.FORBIDDEN);
    }

    /**
     * Just an error message wrapper
     * 
     * @param mensaje
     *            {@link String}
     * @return {@link Map} of {@link String}
     */
    private static Map<String, Object> error(final String mensaje) {
        return Collections.singletonMap("Unexpected error", mensaje);
    }
}
