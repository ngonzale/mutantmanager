package com.ml.api;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import com.ml.domain.DNA;
import com.ml.domain.Person;
import com.ml.repository.DnaRepository;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = DnaRepository.class)
public class MutantResourceTest {

	@Mock
	private DnaRepository dnaRepository;
	
	@Autowired
	@InjectMocks
	@Spy
	private MutantResource mutantResource;
	
	@Test
	public void calculateRatioTest() {
		
		Mockito.when(dnaRepository.findByMutantCondition(true)).thenReturn(40);
		Mockito.when(dnaRepository.count()).thenReturn(100L);
		
		Float ratio = mutantResource.calculateRatio();
		
		Mockito.verify(dnaRepository).findByMutantCondition(true);
		Mockito.verify(dnaRepository).count();
		assertTrue(ratio.equals(0.4F));
	}
	
	
	@Test
	public void checkValidMutantDNATest(){
		
		Person person = Mockito.mock(Person.class);
		DNA dna = new DNA();

		Mockito.when(person.isMutant()).thenReturn(true);
		Mockito.when(person.getAdn()).thenReturn(dna);
		Mockito.doReturn(null).when(dnaRepository).save(dna);
		
		//SUT
		ResponseEntity<?> response = mutantResource.mutant(person);
		
		Mockito.verify(dnaRepository).save(dna);
		assertTrue(response.getStatusCodeValue() == HttpStatus.OK.value());
	}
	
	@Test
	public void checkInValidMutantDNATest(){
		
		Person person = Mockito.mock(Person.class);
		DNA dna = new DNA();

		Mockito.when(person.isMutant()).thenReturn(false);
		Mockito.when(person.getAdn()).thenReturn(dna);
		Mockito.doReturn(null).when(dnaRepository).save(dna);
		
		//SUT
		ResponseEntity<?> response = mutantResource.mutant(person);
		
		Mockito.verify(dnaRepository).save(dna);
		assertTrue(response.getStatusCodeValue() == HttpStatus.FORBIDDEN.value());
	}
	
	@Test
	public void calculateRatioWithBorderValuesTest() {
		Mockito.when(dnaRepository.findByMutantCondition(true)).thenReturn(0);
		Mockito.when(dnaRepository.count()).thenReturn(100L);
		Float ratio = mutantResource.calculateRatio();
		assertTrue(ratio.equals(0F));
	}
	
	@Test
	public void calculateRatioWithBorderValuesIITest() {
		Mockito.when(dnaRepository.findByMutantCondition(true)).thenReturn(0);
		Mockito.when(dnaRepository.count()).thenReturn(0L);
		Float ratio = mutantResource.calculateRatio();
		assertTrue(ratio.equals(0F));
	}
	
	@Test
	public void getStatsWithBorderValues(){
		
		Mockito.when(dnaRepository.findByMutantCondition(true)).thenReturn(0);
		Mockito.when(dnaRepository.count()).thenReturn(100L);
		ResponseEntity<?> response = mutantResource.getStats();
		
		assertTrue(response.getBody().equals(0F));
		assertTrue(response.getStatusCode().value() == HttpStatus.OK.value());
		
		
	}
	
	
}
