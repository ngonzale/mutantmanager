package com.ml.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class DnaMatrixWrapperTest {

	private DnaMatrixWrapper dnaMatrixWrapper;
	private String[] dnaMatrix;


	@Test
	public void buildWrapperInternalMatrix() {

		this.dnaMatrix = new String[] { "TGG", "GGC", "TGC" };
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		
		char[][] internalMatrix = dnaMatrixWrapper.buildInternalMatrix(dnaMatrix);

		int row = 0;
		for (String dnaElement : dnaMatrix) {
			for (int col = 0; col < dnaElement.length(); col++) {
				assertEquals(internalMatrix[row][col], dnaElement.charAt(col));
			}
			row++;
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buildWrapperInternalMatrixWithWrongChars() {
		this.dnaMatrix = new String[] { "XGG", "GGC", "TGZ" };
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void buildWrapperInternalMatrixWithWrongDnaSize() {
		this.dnaMatrix = new String[] { "ATGG", "ATG", "ATG" };
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternI() {
		this.dnaMatrix = new String[] {"ATGCCA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternII() {
		this.dnaMatrix = new String[] {"ACTACG","AAGTGC","TTAGGT","AGGAGG","CGCCAA","GACCCA"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternIII() {
		this.dnaMatrix = new String[] {"ACTACC","ATGTGC","TAAGGC","AAGCGC","CACCAC","GACCCA"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternIV() {
		this.dnaMatrix = new String[] {"ACTACC","ATGTGC","AAAGGC","AAGCGC","AACCAC","ACCCCC"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternV() {
		this.dnaMatrix = new String[] {"AAAAAA","AAAAAA","AAAAAA","GGGGGG","CCCCCC","TTTTTT"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithMutantDNAPatternVI() {
		this.dnaMatrix = new String[] {"ACAAAT","GCACGT","GAAGGC","AAGCCC","AACCAC","ACCGCG"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertTrue(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithNoValidDNAs() {
		this.dnaMatrix = new String[] {"AAAAAG","AAGTGC","ATATGT","AGAAGG","ACCCAA","CCCTTA"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertFalse(dnaMatrixWrapper.isMutantDna());
	}
	
	@Test
	public void IsMutantWithNoValidDNAsII() {
		this.dnaMatrix = new String[] {"GATATG","TACTGC","ACATGT","GCAAGG","ATGTAA","CTCCTG"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertFalse(dnaMatrixWrapper.isMutantDna());
	}
	
	/*only 2 4-sequeneces of the same character are not a valid dna criteria. See hypothesis*/
	@Test
	public void IsMutantWithNoValidDNAsIII() {
		this.dnaMatrix = new String[] {"AAAAAA","CGTCGT","GCAGGC","CTGCCC","TCCCAC","AAAAAA"};
		dnaMatrixWrapper = new DnaMatrixWrapper(dnaMatrix);
		assertFalse(dnaMatrixWrapper.isMutantDna());
	}
	
}
